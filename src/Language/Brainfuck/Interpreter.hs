{-|
Module      : Language.Brainfuck.Interpreter
Description : Simple executable to evaluate BF expressions
Copyright   : (c) Allele Dev, 2014
License     : BSD-3
Maintainer  : allele.dev@gmail.com
Stability   : experimental
Portability : POSIX

Usage examples:

>>> bfh 10 '.+.+'
0
1
>>> bfh 10 '[...].'
0
-}
module Main where

import Control.Monad (void)
import Data.Array (listArray)
import System.Environment (getArgs)

import Language.Brainfuck.Eval (eval)

main :: IO ()
main = do
  args <- getArgs
  case args of
    (size:program:_) -> do
      case (reads size :: [(Int, String)]) of
        [(s,[])] -> void $ eval (mkTape s) program
        _ -> putStrLn $ "error: could not parse size: " ++ size
    _ -> putStrLn "usage: bfh <tapeSize> <program>"
  where mkTape s = listArray (0,s-1) (replicate s 0)
