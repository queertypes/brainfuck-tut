{-|
Module      : Language.Brainfuck.Types
Description : Types used in the implementation of the BF language
Copyright   : (c) Allele Dev, 2014
License     : BSD-3
Maintainer  : allele.dev@gmail.com
Stability   : experimental
Portability : POSIX
-}
module Language.Brainfuck.Types (
  Tape,
  DataPointer,
  ProgramCounter(..),
  Term(..),
  Direction(..)
) where

import Data.Word (Word8)
import Data.Array

-- |`Tape` is an alias for the interpreter storage type
type Tape = Array Int Word8

-- |`DataPointer` is an alias for the memory pointer
type DataPointer = Int

{-|
`ProgramCounter` is a nominal type to track the current instruction
while distinguishing it from `DataPointer`
-}
newtype ProgramCounter = PC Int

-- |`Term` represents the abstract syntax for the BF language
data Term
  = IncDP        -- ^ \>
  | DecDP        -- ^ <
  | OutDP        -- ^ ?
  | IncByte      -- ^ +
  | DecByte      -- ^ \-
  | OutByte      -- ^ .
  | InByte       -- ^ ,
  | JumpForward  -- ^ [
  | JumpBackward -- ^ ]
  deriving (Show, Eq)

-- |`Direction` is used to track which direction we're jumping in
data Direction = Forward | Backward deriving Eq
