module Language.Brainfuck (
  parse,
  eval
) where

import Language.Brainfuck.Parse (parse)
import Language.Brainfuck.Eval (eval)
