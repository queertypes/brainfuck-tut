0.7.0.0

* added support for '?' command
    * this breaks compatibility with programs that use ? as a comment
      character

0.6.0.1

* update the changelog

0.6.0.0

* fix: jump logic now respects nesting

0.5.1.3

* Add changelog

0.5.1.2

* Add homepage, issue tracker, project repo

0.5.1.1

* Fix documentation formatting issues

0.5.1.0

* Initial release
